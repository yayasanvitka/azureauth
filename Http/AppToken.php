<?php
namespace Modules\AzureAuth\Http;

use Modules\AzureAuth\Abstracts\ConnectorAbstract;
use Modules\AzureAuth\Exceptions\AzureAuthException;
use Modules\AzureAuth\Interfaces\ConnectorInterface;

class AppToken extends ConnectorAbstract
{
    public $token;
    protected $clientId;
    protected $clientSecret;
    protected $tenantId;

    protected $cacheTag;
    protected $cacheName;
    protected $cacheLifetime;

    public $requestBody = [];

    public function __construct()
    {
        $this->cacheTag = env('APP_KEY');
        $this->cacheName = '_azureToken';
        $this->cacheLifetime = 3600;
        $this->clientId = config('azureauth.config.ClientId');
        $this->clientSecret = config('azureauth.config.ClientSecret');
        $this->tenantId = config('azureauth.config.TenantId');

        return $this;
    }

    /**
     * Get the token. If the token exists on cache, return the cached token.
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function get()
    {
        if (cache()->tags([$this->cacheTag])->has($this->cacheName) == false) {
            $response = $this->fetch()->getResponse();
            throw_if(isset($response['error']), new AzureAuthException(isset($response['message']) ? $response['message'] : null));

            cache()
                ->tags([$this->cacheTag])
                ->put($this->cacheName, $response['access_token'], $response['expires_in']);
        }

        return collect(['token' => cache()->tags([$this->cacheTag])->get($this->cacheName)]);
    }

    /**
     * Format the data before sending
     * @param array $array
     * @return ConnectorInterface
     */
    public function format(array $array = null) : ConnectorInterface
    {
        $this->requestBody = [
            "grant_type" => "client_credentials",
            "client_id" => config('azureauth.config.ClientId'),
            "client_secret" => config('azureauth.config.ClientSecret'),
            "scope" => "https://graph.microsoft.com/.default",
        ];

        return $this;
    }
}
