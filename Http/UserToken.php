<?php


namespace Modules\AzureAuth\Http;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Modules\AzureAuth\Abstracts\ConnectorAbstract;
use Modules\AzureAuth\Exceptions\AzureAuthException;
use Modules\AzureAuth\Interfaces\ConnectorInterface;
use GuzzleHttp\Client;

class UserToken extends ConnectorAbstract
{
    public $token;
    protected $clientId;
    protected $clientSecret;
    protected $tenantId;

    protected $cacheTag;
    protected $cacheName;
    protected $cacheLifetime;

    public $requestBody = [];

    public function __construct()
    {
        $this->cacheTag = env('APP_KEY');
        $this->cacheName = '_userToken';
        $this->cacheLifetime = 3600;
        $this->clientId = config('azureauth.config.ClientId');
        $this->clientSecret = config('azureauth.config.ClientSecret');
        $this->tenantId = config('azureauth.config.TenantId');

        return $this;
    }

    private function validate(array $array)
    {
        return Validator::make($array, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
    }

    /**
     * Get the token. If the token exists on cache, return the cached token.
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function get()
    {
        $response = $this->fetch()->getResponse();
        throw_if(isset($response['error']), new AzureAuthException(isset($response['message']) ? $response['message'] : null));

        return $response;
    }

    /**
     * Format the data before sending
     * @param array|null $array
     * @return ConnectorInterface
     * @throws \Throwable
     */
    public function format(array $array = null) : ConnectorInterface
    {
        $validator = $this->validate($array);

        throw_if($validator->fails(), new AzureAuthException($validator->getMessageBag()));

        $this->requestBody = [
            "grant_type" => "password",
            "client_id" => config('azureauth.config.ClientId'),
            "client_secret" => config('azureauth.config.ClientSecret'),
            "scope" => "https://graph.microsoft.com/.default",
            "userName" => $array['email'],
            "password" => $array['password']
        ];

        return $this;
    }

    public function me($token)
    {
        $endpoint = 'https://graph.microsoft.com/v1.0/me';
        $client = new Client();

        // make the request
        try {
            $this->response = $client->request('GET', $endpoint, [
                'headers' => [
                   "Authorization" => "Bearer $token",
                ]
            ]);
        } catch (ClientException $exception) {
            $this->response = $exception;
        }

        return $this->getResponseForMe();
    }

    /**
     * Return back the response to application
     * Should be instanceof Illuminate\Support\Collection, so user can adjust the data.
     * @return Collection
     */
    private function getResponseForMe() : Collection
    {
        if ($this->response instanceof ClientException) {
            // get the full content
            $content = json_decode($this->response->getResponse()->getBody()->getContents());

            // create and send response
            return collect([
                'error' => $content->error->code,
                'message' => $content->error->message
            ]);
        }

        if ($this->response->getStatusCode() == 200) {
            $content = collect(json_decode($this->response->getBody(), true));
            return $content;
        }

        return collect(['error' => 'Unknown Error', 'message' => 'An Unknown Error has Occurred']);
    }
}
