<?php


namespace Modules\AzureAuth\Http;

use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Facades\Modules\AzureAuth\Http\UserToken;
use Modules\AzureAuth\Exceptions\AzureAuthException;

class CreateUpdateUser
{
    protected $user;        // user from azure
    protected $existing;    // user from database

    public function iterateUser(Collection $userCollection)
    {
        foreach ($userCollection as $user) {
            if ($this->checkUser($user)) {
                // update if user exists
                $this->updateUser($this->existing);
            } else {
                // create if not exists
                $this->createUser($this->user);
            }
        }
    }

    public function updateUserLoginData($userToken)
    {
        $this->existing = User::whereEmail(request()->email)->first();

        if ($this->existing instanceof User) {
            $this->existing->password = bcrypt(request()->password);
            $this->existing->save();
        } else {
            $me = UserToken::me($userToken);
            throw_if(isset($me['error']), new AzureAuthException(isset($me['message']) ? $me['message'] : null));

            User::create([
                'uuid' => $me['id'],
                'name' => ucwords(strtolower($me['displayName'])),
                'email' => $me['userPrincipalName'],
                'password' => bcrypt(request()->password),
            ]);
        }

        return true;
    }

    private function checkUser($user) : bool
    {
        $this->user = $user;
        $user = User::whereUuid($user['id'])->orWhere('email', $user['mail'])->first();

        if ($user == null) {
            return false;
        } elseif ($user instanceof User) {
            $this->existing = $user;
            return true;
        }
    }

    private function updateUser($user) : User
    {
        $this->existing->uuid = $this->user['id'];
        $this->existing->name = ucwords(strtolower($this->user['displayName']));
        $this->existing->email = $this->user['userPrincipalName'];
        $this->existing->save();

        return $this->existing;
    }

    private function createUser($user) : User
    {
        $u = User::create([
            'uuid' => $user['id'],
            'name' => ucwords(strtolower($user['displayName'])),
            'email' => $user['userPrincipalName'],
            'password' => Hash::make(Str::random(10)),
        ]);

        return $u;
    }
}
