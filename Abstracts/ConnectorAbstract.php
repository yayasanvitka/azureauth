<?php


namespace Modules\AzureAuth\Abstracts;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Modules\AzureAuth\Interfaces\ConnectorInterface;

abstract class ConnectorAbstract implements ConnectorInterface
{
    public $requestBody = [];
    public $requestUrl;
    public $response;

    public $token;

    /**
     * Format the data before sending
     * @param array $array
     * @return ConnectorInterface
     */
    abstract public function format(array $array) : ConnectorInterface;

    /**
     * Connect to Microsoft Azure API
     * @return ConnectorInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetch() : ConnectorInterface
    {
        $endpoint = config('azureauth.endpoint') . config('azureauth.config.TenantId') . config('azureauth.token_url');
        $client = new Client();

        // make the request
        try {
            $this->response = $client->request('POST', $endpoint, [
                'form_params' => $this->requestBody
            ]);
        } catch (ClientException $exception) {
            $this->response = $exception;
        }

        return $this;
    }

    /**
     * Return back the response to application
     * Should be instanceof Illuminate\Support\Collection, so user can adjust the data.
     * @return Collection
     */
    public function getResponse() : Collection
    {
        if ($this->response instanceof ClientException) {
            // get the full content
            $content = json_decode($this->response->getResponse()->getBody()->getContents());

            // create and send response
            $data['error'] = $content->error;
            $data['message'] = explode(PHP_EOL, $content->error_description)[0];
            return collect($data);
        }

        if ($this->response->getStatusCode() == 200) {
            $content = collect(json_decode($this->response->getBody(), true));
            $this->token = $content['access_token'];
            return $content;
        }

        return collect(['error' => 'Unknown Error', 'message' => 'An Unknown Error has Occurred']);
    }
}
