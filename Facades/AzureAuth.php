<?php


namespace Modules\AzureAuth\Facades;

use Illuminate\Support\Facades\Facade;

class AzureAuth extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AzureAuth';
    }
}
