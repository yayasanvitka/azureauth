<?php
namespace Modules\AzureAuth\Interfaces;

use Illuminate\Support\Collection;

interface ConnectorInterface
{
    /**
     * Format the data before sending
     * @param array $array
     * @return ConnectorInterface
     */
    public function format(array $array) : ConnectorInterface;

    /**
     * Connect to Microsoft Azure API
     * @return ConnectorInterface
     */
    public function fetch() : ConnectorInterface;

    /**
     * Return back the response to application
     * Should be instanceof Illuminate\Support\Collection, so user can adjust the data.
     * @return Collection
     */
    public function getResponse() : Collection;
}
