<?php


namespace Modules\AzureAuth\Exceptions;

use Exception;
use Throwable;

class AzureAuthException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        \Log::emergency($this->message);
    }
}
