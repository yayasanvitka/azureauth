<?php


namespace Modules\AzureAuth;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Modules\AzureAuth\Abstracts\ConnectorAbstract;
use Modules\AzureAuth\Http\AppToken;
use Modules\AzureAuth\Http\UserToken;
use Modules\AzureAuth\Http\CreateUpdateUser;
use Modules\AzureAuth\Interfaces\ConnectorInterface;
use Illuminate\Support\Collection;

class AzureAuth extends ConnectorAbstract
{
    protected $appToken;
    protected $userToken;
    public $token;

    public function __construct(AppToken $appToken, UserToken $userToken)
    {
        $this->appToken = $appToken;
        $this->userToken = $userToken;
        return $this;
    }

    /**
     * @param array $arr
     * @return Collection
     * @throws \Throwable
     */
    public function authenticate(array $arr) //: Collection
    {
        $result = $this->userToken->format($arr)->get();
        if (isset($result['access_token'])) {
            $n = new CreateUpdateUser();
            if ($n->updateUserLoginData($result['access_token'])) {
                return true;
            }
        }
    }

    public function getAll()
    {
        $result = $this->appToken->format()->get();

        if (isset($result['token'])) {
            $this->token = $result['token'];
        }

        return $this->format()->fetch()->getResponse();
    }

    public function format(array $array = null): ConnectorInterface
    {
        $this->requestBody = [
           "Authorization" => "Bearer $this->token",
        ];
        return $this;
    }

    /**
     * Connect to Microsoft Azure API
     * @return ConnectorInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetch() : ConnectorInterface
    {
        $endpoint = 'https://graph.microsoft.com/v1.0/users?$top=999&$select=displayName,mail,id,userPrincipalName';
        $client = new Client();

        // make the request
        try {
            $this->response = $client->request('GET', $endpoint, [
                'headers' => $this->requestBody
            ]);
        } catch (ClientException $exception) {
            $this->response = $exception;
        }

        return $this;
    }

    /**
     * Return back the response to application
     * Should be instanceof Illuminate\Support\Collection, so user can adjust the data.
     * @return Collection
     */
    public function getResponse() : Collection
    {
        if ($this->response instanceof ClientException) {
            // get the full content
            $content = json_decode($this->response->getResponse()->getBody()->getContents());

            // create and send response
            $data['error'] = $content->error;
            $data['message'] = explode(PHP_EOL, $content->error_description)[0];
            return collect($data);
        }

        if ($this->response->getStatusCode() == 200) {
            $content = collect(json_decode($this->response->getBody(), true));

            $users = $content['value'];

            $n = new CreateUpdateUser();
            $n->iterateUser(collect($users));

            return collect(['success' => true, 'message' => "User Data synced with Azure!"]);
        }

        return collect(['error' => 'Unknown Error', 'message' => 'An Unknown Error has Occurred']);
    }
}
